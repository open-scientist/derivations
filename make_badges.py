#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from json import loads


# In[ ]:


with open("derivations.json", 'r') as f:
    derivations = loads(f.read())


# In[ ]:


derivations


# In[ ]:


import markdown2


# In[ ]:


get_ipython().system('if [ -f /tmp/badges.md ];then rm /tmp/badges.md;fi')

for quad in derivations:
    S = quad["subject"]["id"]
    P = quad["property"]["id"]
    O = quad["object"]["value"]
    unit = quad["object"]["unit"]
    
    labels = {
        "S": quad["subject"]["label"],
        "P": quad["property"]["label"]
    }
    
    svg_link = "https://gitlab.com/open-scientist/derivations/raw/derivations/badges/{}_{}.svg".format(labels["S"], labels["P"])
    derivation_link = quad["derivated_from"]
    
    markdown = """
## {} {}
[ ![]({}) ]({})

```[ ![]({}) ]({})```

""".format(labels["S"], labels["P"], svg_link, derivation_link, svg_link, derivation_link)
    
    print(markdown)
    
    label_S = labels["S"]
    label_P = labels["P"]
    
    get_ipython().system('badge $label_S": "$label_P" =" $O" "$unit > badges/"$label_S"_"$label_P".svg')
    
    with open("/tmp/badges.md", 'a') as f:
        f.write(markdown + "\n\n")

with open("badges.html", 'w') as g:
    with open("/tmp/badges.md", 'r') as f:
        html = markdown2.markdown(f.read())
        g.write(html) 


# In[ ]:


#!jupyter nbconvert --to script make_badges.ipynb

